Development Board: LPC55S69 <br />
Microphones: SparkFun Bob-12758 <br />
LCD screen: Waveshare 13892 <br /> <br />

This program is used for localization source of sound with two microphones. App use gcc-phat (Generalized Cross-Correlation Phase Transform) and TDOA (Time Difference Of Arrival) algorithm. <br />

![Scheme](images/view.png)
